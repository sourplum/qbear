#ifndef ADDCUSTOMER_H
#define ADDCUSTOMER_H

#include <QDialog>
#include <QtSql>
#include "qbear.h"

namespace Ui {
class addCustomer;
}

class addCustomer : public QDialog
{
    Q_OBJECT
    
public:
    explicit addCustomer(QWidget *parent = 0);
    ~addCustomer();
    
private slots:
    void on_buttonBox_accepted();

    void on_comboBox_country_currentIndexChanged(int index);

signals:
    void completed();

private:
    Ui::addCustomer *ui;
    QPointer<QBear> qbear;
};

#endif // ADDCUSTOMER_H
