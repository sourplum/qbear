#include "invoicedetails.h"
#include "ui_invoicedetails.h"

invoiceDetails::invoiceDetails(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::invoiceDetails)
{
    ui->setupUi(this);
    qbear = new QBear();
}

invoiceDetails::~invoiceDetails()
{
    delete ui;
}

void invoiceDetails::setIndex(const QModelIndex &value)
{
    index = value;
    ui->lineEdit_code->setText(getData(invoicetablename::ID).toString());
    ui->kdatecombobox_date->setDate(getData(invoicetablename::DATE).toDate());
    ui->spinBox_sale->setValue(getData(invoicetablename::SALE).toInt());

    qbear->fillComboboxCustomer(ui->comboBox_customer);
    qbear->fillComboboxEmployee(ui->comboBox_employee);
    qbear->fillComboboxPaymentType(ui->comboBox_payment_type);

    ui->comboBox_payment_type->setCurrentIndex(ui->comboBox_payment_type->findData(getData(invoicetablename::PAYMENT_TYPE).toInt()));
    ui->comboBox_employee->setCurrentIndex(ui->comboBox_employee->findData(getData(invoicetablename::EMPLOYEE_ID).toInt()));
    ui->comboBox_customer->setCurrentIndex(ui->comboBox_customer->findData(getData(invoicetablename::CUSTOMER_ID).toInt()));
}

QVariant invoiceDetails::getData(invoicetablename tag)
{
    return index.sibling(index.row(),(int)tag).data();
}

void invoiceDetails::on_buttonBox_accepted()
{
    QSqlDatabase db = QSqlDatabase::database("ouatelse");
    if (db.isOpen()) {
        QSqlQuery q(db);
        q.prepare("UPDATE oua_facture SET date=:date, moyens_paiements_id=:payment_type, pourcentage_remise=:sale, salaries_id=:employee_id, clients_id=:customer_id WHERE id=:id");
        q.bindValue(":id", ui->lineEdit_code->text());
        q.bindValue(":date",ui->kdatecombobox_date->date());
        q.bindValue(":payment_type",ui->comboBox_payment_type->currentIndex());
        q.bindValue(":sale",ui->spinBox_sale->value());
        q.bindValue(":employee_id", ui->comboBox_employee->itemData(ui->comboBox_employee->currentIndex()));
        q.bindValue(":customer_id", ui->comboBox_customer->itemData(ui->comboBox_customer->currentIndex()));
        q.exec();
    }
    emit completed();
}
