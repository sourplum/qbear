<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>About</name>
    <message>
        <location filename="about.ui" line="14"/>
        <source>About QBear</source>
        <translation>À Propos de QBear</translation>
    </message>
    <message>
        <location filename="about.ui" line="60"/>
        <source>QBear</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="67"/>
        <source>Version 1.0</source>
        <oldsource>Version 0.2</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="74"/>
        <source>Using Qt 4.8.6</source>
        <oldsource>Using Qt 4.8</oldsource>
        <translation>Utilisant Qt 4.8.6</translation>
    </message>
    <message>
        <location filename="about.ui" line="104"/>
        <source>About</source>
        <translation>À Propos</translation>
    </message>
    <message>
        <location filename="about.ui" line="110"/>
        <source>Enterprise Management</source>
        <translation>Gestionnaire d&apos;Entreprise</translation>
    </message>
    <message>
        <location filename="about.ui" line="117"/>
        <source>2014-2015</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="124"/>
        <location filename="about.ui" line="127"/>
        <source>https://bitbucket.org/cedbonhomme/qbear</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="135"/>
        <source>Authors</source>
        <translation>Auteurs</translation>
    </message>
    <message>
        <location filename="about.ui" line="170"/>
        <source>Mathilde Guedon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="156"/>
        <source>Cedric Bonhomme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="163"/>
        <source>Clement Binois</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="184"/>
        <source>Thomas Francisco</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="177"/>
        <source>Oliver Chedeville-Lebouc</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Customer</name>
    <message>
        <location filename="customer.ui" line="14"/>
        <source>Customer Managment</source>
        <translation>Gestion des Clients</translation>
    </message>
    <message>
        <location filename="customer.ui" line="29"/>
        <source>Add Customer</source>
        <translation>Ajouter un Client</translation>
    </message>
    <message>
        <location filename="customer.ui" line="66"/>
        <source>Remove Customer</source>
        <translation>Supprimer un Client</translation>
    </message>
    <message>
        <location filename="customer.ui" line="41"/>
        <source>Edit Customer</source>
        <translation>Éditer un Client</translation>
    </message>
    <message>
        <location filename="customer.cpp" line="45"/>
        <source>Error connection</source>
        <translation>Erreur de connexion</translation>
    </message>
    <message>
        <location filename="customer.cpp" line="45"/>
        <source>Unable to connect to database.</source>
        <translation>Impossible de se connecter à la base de données.</translation>
    </message>
    <message>
        <location filename="customer.cpp" line="76"/>
        <source>Nothing selected</source>
        <translation>Rien n&apos;a été sélectionné</translation>
    </message>
    <message>
        <location filename="customer.cpp" line="76"/>
        <source>No row selected, please try again.</source>
        <translation>Aucune ligne sélectionné, merci de réessayer.</translation>
    </message>
</context>
<context>
    <name>Employee</name>
    <message>
        <location filename="employee.ui" line="14"/>
        <source>Employee Managment</source>
        <translation>Gestion des Salariés</translation>
    </message>
    <message>
        <location filename="employee.ui" line="29"/>
        <source>Add Employee</source>
        <translation>Ajouter un Salarié</translation>
    </message>
    <message>
        <location filename="employee.ui" line="41"/>
        <source>Edit Employee</source>
        <translation>Éditer un Salarié</translation>
    </message>
    <message>
        <location filename="employee.ui" line="66"/>
        <source>Remove Employee</source>
        <translation>Supprimer un Salarié</translation>
    </message>
    <message>
        <location filename="employee.cpp" line="43"/>
        <source>Error connection</source>
        <translation>Erreur de connexion</translation>
    </message>
    <message>
        <location filename="employee.cpp" line="43"/>
        <source>Unable to connect to database.</source>
        <translation>Impossible de se connecter à la base de données.</translation>
    </message>
    <message>
        <location filename="employee.cpp" line="82"/>
        <source>Nothing selected</source>
        <translation>Rien n&apos;a été sélectionné</translation>
    </message>
    <message>
        <location filename="employee.cpp" line="82"/>
        <source>No row selected, please try again.</source>
        <translation>Aucune ligne sélectionné, merci de réessayer.</translation>
    </message>
</context>
<context>
    <name>Invoice</name>
    <message>
        <location filename="invoice.ui" line="14"/>
        <source>Invoice Management</source>
        <translation>Gestion des Factures</translation>
    </message>
    <message>
        <location filename="invoice.ui" line="29"/>
        <source>Add Invoice</source>
        <translation>Ajouter une Facture</translation>
    </message>
    <message>
        <location filename="invoice.ui" line="66"/>
        <source>Remove Invoice</source>
        <translation>Supprimer une Facture</translation>
    </message>
    <message>
        <location filename="invoice.ui" line="41"/>
        <source>Edit Invoice</source>
        <translation>Éditer une Facture</translation>
    </message>
    <message>
        <location filename="invoice.cpp" line="43"/>
        <source>Error connection</source>
        <translation>Erreur de connexion</translation>
    </message>
    <message>
        <location filename="invoice.cpp" line="43"/>
        <source>Unable to connect to database.</source>
        <translation>Impossible de se connecter à la base de données.</translation>
    </message>
    <message>
        <location filename="invoice.cpp" line="74"/>
        <source>Nothing selected</source>
        <translation>Rien n&apos;a été sélectionné</translation>
    </message>
    <message>
        <location filename="invoice.cpp" line="74"/>
        <source>No row selected, please try again.</source>
        <translation>Aucune ligne sélectionné, merci de réessayer.</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="login.ui" line="14"/>
        <source>Login</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="login.ui" line="63"/>
        <source>&amp;Login:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="login.ui" line="73"/>
        <location filename="login.ui" line="90"/>
        <source>admin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="login.ui" line="80"/>
        <source>Password:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="login.cpp" line="48"/>
        <source>Permission denied</source>
        <translation>Autorisation refusée</translation>
    </message>
    <message>
        <location filename="login.cpp" line="48"/>
        <source>Possibly incorrect password, please try again.</source>
        <translation>Mot de passe incorrect, merci de réessayer.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <source>QBear</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="62"/>
        <source>Checkout</source>
        <translation>Caisse</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="79"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Checkout&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Caisse&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="113"/>
        <source>Me</source>
        <translation>Moi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="140"/>
        <source>Date</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="189"/>
        <source>Client</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="246"/>
        <source>New Customer</source>
        <translation>Nouveau Client</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="286"/>
        <source>Product</source>
        <translation>Produit</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="323"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="361"/>
        <source>Payment Type</source>
        <translation>Moyen de paiement</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="371"/>
        <source>Discount</source>
        <translation>Réduction</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="378"/>
        <source> %</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <source>TOTAL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="411"/>
        <source> €</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="465"/>
        <source>Acco&amp;unt</source>
        <translation>C&amp;ompte</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="472"/>
        <source>&amp;Customers</source>
        <translation>&amp;Clients</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="478"/>
        <source>P&amp;roducts</source>
        <translation>P&amp;roduits</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="484"/>
        <source>In&amp;voices</source>
        <translation>Fac&amp;tures</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="490"/>
        <source>He&amp;lp</source>
        <translation>Aid&amp;e</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="496"/>
        <source>Settin&amp;gs</source>
        <translation>Param&amp;ètres</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="502"/>
        <source>E&amp;mployees</source>
        <translation>S&amp;alariés</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="516"/>
        <source>Main Toolbar</source>
        <translation>Barre Principale</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="551"/>
        <source>&amp;Login</source>
        <translation>&amp;Connexion</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="567"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Déconnexion</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="580"/>
        <source>&amp;View Customers</source>
        <translation>&amp;Afficher Clients</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="593"/>
        <source>&amp;View Products</source>
        <translation>&amp;Afficher Produits</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="603"/>
        <source>&amp;View Invoices</source>
        <translation>&amp;Afficher Factures</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="613"/>
        <source>&amp;About QBear</source>
        <translation>À &amp;propos de QBear</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="623"/>
        <source>&amp;Settings</source>
        <translation>&amp;Paramètres</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="633"/>
        <source>&amp;View Employees</source>
        <translation>&amp;Afficher Salariés</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="583"/>
        <source>View Customers</source>
        <translation>Afficher les Clients</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>id</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>Quantity</source>
        <translation>Quantité</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>Price</source>
        <translation>Prix</translation>
    </message>
</context>
<context>
    <name>Product</name>
    <message>
        <location filename="product.ui" line="14"/>
        <source>Product Management</source>
        <translation>Gestion des Produits</translation>
    </message>
    <message>
        <location filename="product.ui" line="29"/>
        <source>Add Product</source>
        <translation>Ajouter un Produit</translation>
    </message>
    <message>
        <location filename="product.ui" line="66"/>
        <source>Remove Product</source>
        <translation>Supprimer un Produit</translation>
    </message>
    <message>
        <location filename="product.ui" line="41"/>
        <source>Edit Product</source>
        <translation>Éditer un Produit</translation>
    </message>
    <message>
        <location filename="product.cpp" line="43"/>
        <source>Error connection</source>
        <translation>Erreur de connexion</translation>
    </message>
    <message>
        <location filename="product.cpp" line="43"/>
        <source>Unable to connect to database.</source>
        <translation>Impossible de se connecter à la base de données.</translation>
    </message>
    <message>
        <location filename="product.cpp" line="82"/>
        <source>Nothing selected</source>
        <translation>Rien n&apos;a été sélectionné</translation>
    </message>
    <message>
        <location filename="product.cpp" line="82"/>
        <source>No row selected, please try again.</source>
        <translation>Aucune ligne sélectionné, merci de réessayer.</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="settings.ui" line="14"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="settings.ui" line="36"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;General&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Général&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="settings.ui" line="43"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Language&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Langue&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="settings.ui" line="71"/>
        <source>Available translations:</source>
        <translation>Traductions disponibles:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="84"/>
        <source>In order to change language, you must restart Qbear</source>
        <translation>Afin de changer de langue, vous devez redémarrer Qbear</translation>
    </message>
    <message>
        <location filename="settings.ui" line="111"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Appearance&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Apparence&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="settings.ui" line="135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Database&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Base de données&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="settings.ui" line="146"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="settings.ui" line="152"/>
        <source>Hostname</source>
        <translation>Nom de l&apos;hôte</translation>
    </message>
    <message>
        <location filename="settings.ui" line="162"/>
        <source>Database Name</source>
        <translation>Nom de la base</translation>
    </message>
    <message>
        <location filename="settings.ui" line="172"/>
        <source>Username</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <location filename="settings.ui" line="182"/>
        <source>Password</source>
        <translation>Mot de Passe</translation>
    </message>
    <message>
        <location filename="settings.ui" line="241"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="settings.ui" line="251"/>
        <source>Appearance</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <location filename="settings.ui" line="261"/>
        <source>Database</source>
        <translation>Base de données</translation>
    </message>
</context>
<context>
    <name>addCustomer</name>
    <message>
        <location filename="addcustomer.ui" line="14"/>
        <source>Add a customer</source>
        <translation>Ajouter un client</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Add a customer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Ajouter un client&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="54"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="68"/>
        <source>Civility</source>
        <translation>Civilité</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="76"/>
        <source>Mr</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="81"/>
        <source>Mrs</source>
        <translation>Mme</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="86"/>
        <source>Ms</source>
        <translation>Mlle</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="94"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="101"/>
        <source>Customer NAME</source>
        <translation>NOM Client</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="108"/>
        <source>Firstname</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="115"/>
        <source>Customer firstname</source>
        <translation>prénom Client</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="122"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="132"/>
        <source>customer@host.com</source>
        <translation>client@domaine.com</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="139"/>
        <source>Birthday</source>
        <translation>Anniversaire</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="146"/>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="153"/>
        <source>0510213243</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="160"/>
        <source>Mobile</source>
        <translation>Portable</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="167"/>
        <source>0642536475</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="189"/>
        <source>Adress 1</source>
        <translation>Adresse 1</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="196"/>
        <source>Customer address 1</source>
        <translation>adresse 1 Client</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="203"/>
        <source>Adress 2</source>
        <translation>Adresse 2</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="210"/>
        <source>Customer address 2</source>
        <translation>adresse 2 Client</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="217"/>
        <source>Country</source>
        <translation>Pays</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="224"/>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="260"/>
        <source>Note</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>addEmployee</name>
    <message>
        <location filename="addemployee.ui" line="14"/>
        <source>Add an employee</source>
        <translation>Ajouter un Salarié</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Add an employee&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Ajouter un salarié&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="39"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="53"/>
        <source>Civility</source>
        <translation>Civilité</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="61"/>
        <source>Mr</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="66"/>
        <source>Mrs</source>
        <translation>Mme</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="71"/>
        <source>Ms</source>
        <translation>Mlle</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="79"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="86"/>
        <source>NAME</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="93"/>
        <source>Firstname</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="100"/>
        <source>firstname</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="107"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="117"/>
        <source>employee@host.com</source>
        <translation>salarié@domaine.com</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="124"/>
        <source>Birthday</source>
        <translation>Anniversaire</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="134"/>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="141"/>
        <source>0510213243</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="148"/>
        <source>Mobile</source>
        <translation>Portable</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="155"/>
        <source>0642536475</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="174"/>
        <source>Adress 1</source>
        <translation>Adresse 1</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="181"/>
        <source>main address</source>
        <translation>Adresse principale</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="188"/>
        <source>Adress 2</source>
        <translation>Adresse 2</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="195"/>
        <source>second address</source>
        <translation>Adresse secondaire</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="202"/>
        <source>Country</source>
        <translation>Pays</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="216"/>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="243"/>
        <source>Store</source>
        <translation>Magasin</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="253"/>
        <source>Role</source>
        <translation>Rôle</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="275"/>
        <source>Login</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="282"/>
        <source>login</source>
        <translation>connexion</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="289"/>
        <source>Password</source>
        <translation>Mot de Passe</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="302"/>
        <source>password</source>
        <translation>mot de Passe</translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="324"/>
        <source>Note</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>addInvoice</name>
    <message>
        <location filename="addinvoice.ui" line="14"/>
        <source>Add Invoice</source>
        <translation>Ajouter une Facture</translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Add Invoice&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Ajouter une facture&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="54"/>
        <source>Code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="68"/>
        <source>Date</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="75"/>
        <source>Payment type</source>
        <translation>Moyen de paiement</translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="86"/>
        <source>Cheque</source>
        <translation>Chèque</translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="91"/>
        <source>Change</source>
        <translation>Monnaie</translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="96"/>
        <source>Credit Card</source>
        <translation>Carte de crédit</translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="119"/>
        <source>Sale</source>
        <translation>Réduction</translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="126"/>
        <source> %</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="133"/>
        <source>Employee</source>
        <translation>Salarié</translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="147"/>
        <source>Customer</source>
        <translation>Client</translation>
    </message>
</context>
<context>
    <name>addProduct</name>
    <message>
        <location filename="addproduct.ui" line="14"/>
        <source>Add a Product</source>
        <oldsource>Product Details</oldsource>
        <translation>Ajouter un Produit</translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Add a Product&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Product Details&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Ajouter un Produit&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="54"/>
        <source>Code EAN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="68"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="75"/>
        <source>Product Name</source>
        <translation>Nom du produit</translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="94"/>
        <source>Buying Price</source>
        <translation>Prix d&apos;Achat</translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="101"/>
        <source>Selling Price</source>
        <translation>Prix de Vente</translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="108"/>
        <source>Taxes</source>
        <translation>TVA</translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="115"/>
        <location filename="addproduct.ui" line="122"/>
        <location filename="addproduct.ui" line="129"/>
        <source> €</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="146"/>
        <source>Note</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>customerDetails</name>
    <message>
        <location filename="customerdetails.ui" line="14"/>
        <source>Customer Details</source>
        <translation>Détails du Client</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Customer Details&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Détails du Client&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="54"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="68"/>
        <source>Civility</source>
        <translation>Civilité</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="76"/>
        <source>Mr</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="81"/>
        <source>Mrs</source>
        <translation>Mme</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="86"/>
        <source>Ms</source>
        <translation>Mlle</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="94"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="104"/>
        <source>Firstname</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="114"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="128"/>
        <source>Birthday</source>
        <translation>Anniversaire</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="135"/>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="145"/>
        <source>Mobile</source>
        <translation>Portable</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="170"/>
        <source>Adress 1</source>
        <translation>Adresse 1</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="180"/>
        <source>Adress 2</source>
        <translation>Adresse 2</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="190"/>
        <source>Country</source>
        <translation>Pays</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="197"/>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="228"/>
        <source>Invoices</source>
        <translation>Factures</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="238"/>
        <source>Statistics</source>
        <translation>Statistiques</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="244"/>
        <source>Buy Frequency</source>
        <translation>Fréquence d&apos;Achat</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="257"/>
        <location filename="customerdetails.ui" line="277"/>
        <source> %</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="264"/>
        <source>Quantity Per Month</source>
        <translation>Quantité Par Mois</translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="285"/>
        <source>Note</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>employeeDetails</name>
    <message>
        <location filename="employeedetails.ui" line="14"/>
        <source>Employee Details</source>
        <translation>Détails du Salarié</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Employee Details&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Détails du Salarié&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="39"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="53"/>
        <source>Civility</source>
        <translation>Civilité</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="61"/>
        <source>Mr</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="66"/>
        <source>Mrs</source>
        <translation>Mme</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="71"/>
        <source>Ms</source>
        <translation>Mlle</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="79"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="86"/>
        <source>NAME</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="93"/>
        <source>Firstname</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="100"/>
        <source>firstname</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="107"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="117"/>
        <source>employee@host.com</source>
        <translation>salarié@domaine.com</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="124"/>
        <source>Birthday</source>
        <translation>Anniversaire</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="134"/>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="141"/>
        <source>0510213243</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="148"/>
        <source>Mobile</source>
        <translation>Portable</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="155"/>
        <source>0642536475</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="174"/>
        <source>Adress 1</source>
        <translation>Adresse 1</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="181"/>
        <source>main address</source>
        <translation>Adresse principale</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="188"/>
        <source>Adress 2</source>
        <translation>Adresse 2</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="195"/>
        <source>second address</source>
        <translation>Adresse secondaire</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="202"/>
        <source>Country</source>
        <translation>Pays</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="216"/>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="243"/>
        <source>Store</source>
        <translation>Magasin</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="253"/>
        <source>Role</source>
        <translation>Rôle</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="275"/>
        <source>Login</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="282"/>
        <source>login</source>
        <translation>connexion</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="289"/>
        <source>Password</source>
        <translation>Mot de Passe</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="302"/>
        <source>password</source>
        <translation>mot de Passe</translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="324"/>
        <source>Note</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>invoiceDetails</name>
    <message>
        <location filename="invoicedetails.ui" line="14"/>
        <source>Invoice Details</source>
        <translation>Détails de la Facture</translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Invoice Details&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Détails de la Facture&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="54"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="68"/>
        <source>Date</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="75"/>
        <source>Payment type</source>
        <translation>Moyen de paiement</translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="104"/>
        <source>Sale</source>
        <translation>Réduction</translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="111"/>
        <source> %</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="118"/>
        <source>Employee</source>
        <translation>Salarié</translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="132"/>
        <source>Customer</source>
        <translation>Client</translation>
    </message>
</context>
<context>
    <name>productDetails</name>
    <message>
        <location filename="productdetails.ui" line="14"/>
        <source>Product Details</source>
        <oldsource>Customer Details</oldsource>
        <translation>Détails du Produit</translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Product Details&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Détails du Produit&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="57"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="71"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="93"/>
        <source>Buying Price</source>
        <translation>Prix d&apos;Achat</translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="100"/>
        <source>Selling Price</source>
        <translation>Prix de Vente</translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="107"/>
        <source>Taxes</source>
        <translation>TVA</translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="114"/>
        <location filename="productdetails.ui" line="121"/>
        <location filename="productdetails.ui" line="128"/>
        <source> €</source>
        <translation></translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="145"/>
        <source>Note</source>
        <translation></translation>
    </message>
</context>
</TS>
