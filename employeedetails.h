#ifndef EMPLOYEEDETAILS_H
#define EMPLOYEEDETAILS_H

#include <QDialog>
#include <QPointer>
#include <QSqlTableModel>
#include <QtSql>
#include "qbear.h"

namespace Ui {
class employeeDetails;
}

enum class employeetablename {
    ID,NAME,FIRSTNAME,LOGIN,PASSWORD,ADDRESS1,ADDRESS2,PHONE,MOBILE,EMAIL,BIRTHDAY,NOTE,CIVILITY,CITY,NATIONALITY,ROLE,STORE
};

class employeeDetails : public QDialog
{
    Q_OBJECT

public:
    explicit employeeDetails(QWidget *parent = 0);
    ~employeeDetails();

    void setIndex(const QModelIndex &value);
    QVariant getData(employeetablename tag);

private slots:

    void on_comboBox_country_currentIndexChanged(int index);

    void on_buttonBox_accepted();

signals:
    void completed();

private:
    Ui::employeeDetails *ui;
    QPointer<QBear> qbear;
    QSqlDatabase db;
    QModelIndex index;
};

#endif // EMPLOYEEDETAILS_H
