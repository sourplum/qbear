#include <QPointer>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "login.h"
#include "customer.h"
#include "product.h"
#include "invoice.h"
#include "about.h"
#include "settings.h"
#include "addcustomer.h"
#include "employee.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    db = QSqlDatabase::addDatabase("QMYSQL","ouatelse");

    db.setHostName(settings.value("database/hostname").toString());
    db.setDatabaseName(settings.value("database/name").toString());
    db.setUserName(settings.value("database/username").toString());
    db.setPassword(settings.value("database/password").toString());
    qbear = new QBear();

    qbear->fillComboboxCustomer(ui->comboBox_customer);
    qbear->fillComboboxPaymentType(ui->comboBox_payment_type);
    qbear->fillComboboxProduct(ui->comboBox_product);

    checkout_model = new QStandardItemModel(this);
    checkout_model->horizontalHeaderItem(4);
    checkout_model_header << tr("id") << tr("Quantity") << tr("Name") << tr("Description") << tr("Price");
    checkout_model->setHorizontalHeaderLabels(checkout_model_header);

    ui->tableView->setModel(checkout_model);
    ui->tableView->hideColumn(0);
}

MainWindow::~MainWindow()
{
    if (db.isOpen()) {
        db.close();
    }
    delete ui;
}

void MainWindow::on_actionLogin_triggered()
{
    QPointer<Login> login = new Login(this);
    connect(login, SIGNAL(connected()), this, SLOT(onSuccessfulLogin()));
    connect(login, SIGNAL(isCEO(int)), this, SLOT(onGloriousCEOConnect(int)));
    connect(login, SIGNAL(isDealer(int)), this, SLOT(onPeasantDealerConnect(int)));
    login->exec();
}

void MainWindow::on_actionView_Customers_triggered()
{
    QPointer<Customer> customer = new Customer(this);
    connect(customer, SIGNAL(changeOccured()), this, SLOT(updateCustomerList()));
    customer->show();
}

void MainWindow::on_actionView_Products_triggered()
{
    QPointer<Product> product = new Product(this);
    connect(product, SIGNAL(changeOccured()), this, SLOT(updateProductList()));
    product->show();
}

void MainWindow::on_actionView_Invoices_triggered()
{
    QPointer<Invoice> invoice = new Invoice(this);
    connect(invoice, SIGNAL(changeOccured()), this, SLOT(updateInvoiceList()));
    invoice->show();
}

void MainWindow::on_actionDisconnect_triggered()
{
    ui->actionDisconnect->setEnabled(false);
    ui->actionView_Customers->setEnabled(false);
    ui->actionView_Employee->setEnabled(false);
    ui->actionView_Invoices->setEnabled(false);
    ui->actionView_Products->setEnabled(false);
    ui->page_checkout->setEnabled(false);
}

void MainWindow::on_actionAbout_QBear_triggered()
{
    QPointer<About> about = new About(this);
    about->show();
}

void MainWindow::on_actionSettings_triggered()
{
    QPointer<Settings> settings = new Settings(this);
    settings->show();
}

void MainWindow::on_listWidget_currentRowChanged(int currentRow)
{
    ui->stackedWidget->setCurrentIndex(currentRow);
}

void MainWindow::on_pushButton_add_customer_clicked()
{
    QPointer<addCustomer> addcustomer = new addCustomer(this);
    connect(addcustomer, SIGNAL(completed()), this, SLOT(updateCustomerList()));
    addcustomer->exec();
}

void MainWindow::on_buttonBox_accepted()
{
    if (db.isOpen()) {
        QSqlQuery q(db);
        q.prepare("INSERT INTO oua_facture (date, moyens_paiements_id, pourcentage_remise, salaries_id, clients_id) "
                  "VALUES (:date, :payment_type, :sale, :employee_id, :customer_id)");
        q.bindValue(":date",ui->kdatecombobox_date->date());
        q.bindValue(":payment_type",ui->comboBox_payment_type->currentIndex());
        q.bindValue(":sale",ui->spinBox_sale->value());
        q.bindValue(":employee_id", employeeId);
        q.bindValue(":customer_id", ui->comboBox_customer->itemData(ui->comboBox_customer->currentIndex()));
        q.exec();

        QSqlQuery query_invoice_id(db);
        int invoice_inserted_id;
        query_invoice_id.exec("SELECT LAST_INSERT_ID() FROM oua_facture");
        while (query_invoice_id.next()) {
            invoice_inserted_id = query_invoice_id.value(0).toInt();
        }

        QSqlQuery query(db);
        for (int row = 0; row < checkout_model->rowCount(); ++row) {
            query.prepare("INSERT INTO oua_facture_produit (quantite, factures_id, produits_id) "
                          "VALUES (:quantity, :invoice_id, :product_id)");
            query.bindValue(":quantity", checkout_model->item(row,0)->text());
            query.bindValue(":invoice_id", invoice_inserted_id);
            query.bindValue(":product_id", checkout_model->item(row,1)->text());
            query.exec();
        }
    }
}


void MainWindow::on_pushButton_add_product_to_checkout_clicked()
{
    if (ui->comboBox_product->currentIndex() > -1) {
        QList< QStandardItem* > row;
        int product_id = ui->comboBox_product->itemData(ui->comboBox_product->currentIndex()).toInt();
        QVariant product_price = qbear->getProductPrice(product_id);

        checkout_model->appendRow(row << new QStandardItem(QString::number(product_id))
                                  << new QStandardItem(QString::number(ui->spinBox_quantity->value()))
                                  << new QStandardItem(ui->comboBox_product->currentText())
                                  << new QStandardItem(qbear->getProductDescription(product_id))
                                  << new QStandardItem(product_price.toString()));
        total_product = ui->doubleSpinBox_total_price->value() + product_price.toDouble()*ui->spinBox_quantity->value();
        ui->doubleSpinBox_total_price->setValue(total_product);
        ui->tableView->resizeColumnsToContents();
    }
}

void MainWindow::on_actionView_Employee_triggered()
{
    QPointer<Employee> employee = new Employee(this);
    connect(employee, SIGNAL(changeOccured()), this, SLOT(updateEmployeeList()));
    employee->show();
}

void MainWindow::on_spinBox_sale_valueChanged(int arg1)
{
    ui->doubleSpinBox_total_price->setValue( total_product - (total_product*arg1)/100 );
}

void MainWindow::on_buttonBox_rejected()
{

}

void MainWindow::on_buttonBox_clicked(QAbstractButton *button)
{
    switch (ui->buttonBox->standardButton(button)) {
    case QDialogButtonBox::Reset:
        checkout_model->clear();
        checkout_model->setHorizontalHeaderLabels(checkout_model_header);
        ui->comboBox_customer->setCurrentIndex(-1);
        ui->comboBox_payment_type->setCurrentIndex(-1);
        ui->comboBox_product->setCurrentIndex(-1);
        ui->doubleSpinBox_total_price->setValue(0);
        total_product = 0;
        ui->spinBox_quantity->setValue(1);
        ui->spinBox_sale->setValue(0);
        break;
    default:
        break;
    }
}

void MainWindow::on_toolButton_clicked()
{
    ui->comboBox_customer->setCurrentIndex(-1);
}

void MainWindow::updateCustomerList()
{
    qbear->fillComboboxCustomer(ui->comboBox_customer);
}

void MainWindow::updateProductList()
{
    qbear->fillComboboxProduct(ui->comboBox_product);
}

void MainWindow::updateEmployeeList()
{
    // DO NOTHING
}

void MainWindow::updateInvoiceList()
{
    // DO SOMETHING
}

void MainWindow::onSuccessfulLogin()
{
    ui->actionDisconnect->setEnabled(true);
}

void MainWindow::onPeasantDealerConnect(int idDealer)
{
    employeeId = idDealer;
    ui->actionView_Invoices->setEnabled(true);
    ui->actionView_Products->setEnabled(true);
    ui->page_checkout->setEnabled(true);
    ui->lineEdit_employee_connected->setText(qbear->getEmployeeName(idDealer));
}

void MainWindow::onGloriousCEOConnect(int idCEO)
{
    employeeId = idCEO;
    ui->actionView_Customers->setEnabled(true);
    ui->actionView_Employee->setEnabled(true);
    ui->actionView_Invoices->setEnabled(true);
    ui->actionView_Products->setEnabled(true);
    ui->page_checkout->setEnabled(true);
    ui->lineEdit_employee_connected->setText(qbear->getEmployeeName(idCEO));
}
