#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <QDialog>
#include <QtSql>
#include <QPointer>
#include <QMessageBox>

namespace Ui {
class Employee;
}

class Employee : public QDialog
{
    Q_OBJECT

public:
    explicit Employee(QWidget *parent = 0);
    ~Employee();

    void deleteEmployee();

private slots:
    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_pushButton_add_clicked();

    void on_pushButton_remove_clicked();

    void on_pushButton_edit_clicked();

public slots:
    void updateTableView();

signals:
    void changeOccured();

private:
    Ui::Employee *ui;
    QPointer<QSqlTableModel> model;
    QSqlDatabase db;
};

#endif // EMPLOYEE_H
