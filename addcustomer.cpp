#include "addcustomer.h"
#include "ui_addcustomer.h"

addCustomer::addCustomer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addCustomer)
{
    ui->setupUi(this);
    qbear = new QBear();

    qbear->fillComboboxCountry(ui->comboBox_country);
}

addCustomer::~addCustomer()
{
    delete ui;
}

void addCustomer::on_buttonBox_accepted()
{
    QSqlDatabase db = QSqlDatabase::database("ouatelse");
    if (db.isOpen()) {
        QSqlQuery q(db);
        q.prepare("INSERT INTO oua_client (nom, prenom, adresse1, adresse2, fixe, portable, mail, naissance, notes, nationalite_id, villes_id, civilite_id) "
                  "VALUES (:name, :firstname, :adresseFirst, :adresseSecond, :phone, :mobile, :mail, :birthday, :notes, :idNation, :idCity, :idCivil)");
        q.bindValue(":name", ui->lineEdit_name->text());
        q.bindValue(":firstname", ui->lineEdit_firstname->text());
        q.bindValue(":adresseFirst", ui->lineEdit_address1->text());
        q.bindValue(":adresseSecond", ui->lineEdit_address2->text());
        q.bindValue(":phone", ui->lineEdit_phone->text());
        q.bindValue(":mobile", ui->lineEdit_mobile->text());
        q.bindValue(":mail", ui->lineEdit_email->text());
        q.bindValue(":birthday", ui->kdatecombobox_birthday->date());
        q.bindValue(":notes", ui->textEdit_Note->toPlainText());
        q.bindValue(":idNation", ui->comboBox_country->itemData(ui->comboBox_country->currentIndex()));
        q.bindValue(":idCity", ui->comboBox_city->itemData(ui->comboBox_city->currentIndex()));
        q.bindValue(":idCivil", ui->comboBox_civility->currentIndex());
        q.exec();
    }
    emit completed();
}

void addCustomer::on_comboBox_country_currentIndexChanged(int index)
{
    qbear->fillComboboxCity(ui->comboBox_city, ui->comboBox_country->itemData(index).toInt());
}
