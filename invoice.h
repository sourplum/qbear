#ifndef INVOICE_H
#define INVOICE_H

#include <QDialog>
#include <QtSql>
#include <QPointer>
#include <QMessageBox>

namespace Ui {
class Invoice;
}

class Invoice : public QDialog
{
    Q_OBJECT
    
public:
    explicit Invoice(QWidget *parent = 0);
    ~Invoice();

    void deleteInvoice();
    
private slots:
    void on_pushButton_add_clicked();

    void on_pushButton_remove_clicked();

    void on_pushButton_edit_clicked();

    void on_tableView_doubleClicked(const QModelIndex &index);

public slots:
    void updateTableView();

signals:
    void changeOccured();

private:
    Ui::Invoice *ui;
    QPointer<QSqlTableModel> model;
    QSqlDatabase db;
};

#endif // INVOICE_H
