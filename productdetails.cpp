#include "productdetails.h"
#include "ui_productdetails.h"

productDetails::productDetails(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::productDetails)
{
    ui->setupUi(this);
    qbear = new QBear();
}

productDetails::~productDetails()
{
    delete ui;
}
QModelIndex productDetails::getIndex() const
{
    return index;
}

void productDetails::setIndex(const QModelIndex &value)
{
    index = value;
    ui->lineEdit_name->setText(getData(producttablename::NAME).toString());
    ui->lineEdit_code->setText(getData(producttablename::ID).toString());
    ui->textEdit_note->setText(getData(producttablename::DESCRIPTION).toString());
    ui->doubleSpinBox_buy->setValue(getData(producttablename::BUYINGPRICE).toDouble());
    ui->doubleSpinBox_sell->setValue(getData(producttablename::SELLINGPRICE).toDouble());
    ui->doubleSpinBox_tax->setValue(getData(producttablename::TAXE).toDouble());
}

QVariant productDetails::getData(producttablename tag)
{
    return index.sibling(index.row(),(int)tag).data();
}


void productDetails::on_buttonBox_accepted()
{
    QSqlDatabase db = QSqlDatabase::database("ouatelse");
    if (db.isOpen()) {
        QSqlQuery q(db);
        q.prepare("UPDATE oua_produit SET nom=:name, designation=:description, prix_achat=:buy, prix_vente=:sell, tva=:tax WHERE id=:id");
        q.bindValue(":id", ui->lineEdit_code->text());
        q.bindValue(":name", ui->lineEdit_name->text());
        q.bindValue(":description", ui->textEdit_note->toPlainText());
        q.bindValue(":buy", ui->doubleSpinBox_buy->value());
        q.bindValue(":sell", ui->doubleSpinBox_sell->value());
        q.bindValue(":tax", ui->doubleSpinBox_tax->value());
        q.exec();
    }
    emit completed();
}

