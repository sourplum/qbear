#include "customer.h"
#include "ui_customer.h"
#include "addcustomer.h"
#include "customerdetails.h"


Customer::Customer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Customer)
{
    ui->setupUi(this);

    db = QSqlDatabase::database("ouatelse");
    if (db.isOpen()) {
        model = new QSqlTableModel(this, db);
        model->setTable("oua_client");
        model->select();
        ui->tableView->setModel(model);
        ui->tableView->hideColumn(0);
    } else {
        if (!model.isNull()) {
            model->clear();
        }
    }

}

Customer::~Customer()
{
    delete ui;
}

void Customer::deleteCustomer()
{
    QSqlQuery query(db);
    QModelIndex currentIndex = ui->tableView->currentIndex();
    int id = currentIndex.sibling(currentIndex.row(),0).data().toInt() ;

    if (db.isOpen()) {
        query.prepare("delete from oua_client where oua_client.id = :id") ;
        query.bindValue(":id", id);
        query.exec();
        updateTableView();
    } else{
        QMessageBox::warning(this, tr("Error connection"), tr("Unable to connect to database."), QMessageBox::Ok);
    }
}

void Customer::on_pushButton_clicked()
{
    QPointer<addCustomer> addcustomer = new addCustomer(this);
    connect(addcustomer,SIGNAL(completed()),this,SLOT(updateTableView()));
    addcustomer->exec();
}

void Customer::on_removeButton_clicked()
{
    QPointer<QItemSelectionModel> select = ui->tableView->selectionModel();
    if (select->hasSelection()) {
        QMessageBox msgBox;
        msgBox.setText("A customer will be deleted.");
        msgBox.setInformativeText("Do you want to continue?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);

        switch (msgBox.exec()) {
        case QMessageBox::Yes:
            deleteCustomer();
            break;
        case QMessageBox::No:
            break;
        default:
            break;
        }
    } else {
        QMessageBox::warning(this, tr("Nothing selected"), tr("No row selected, please try again."), QMessageBox::Ok);
    }
}

void Customer::on_tableView_doubleClicked(const QModelIndex &index)
{
    QPointer<customerDetails> customerdetails = new customerDetails(this);
    customerdetails->setIndex(index);
    connect(customerdetails,SIGNAL(completed()),this,SLOT(updateTableView()));
    customerdetails->exec();
}

void Customer::on_pushButton_3_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
    QPointer<customerDetails> customerdetails = new customerDetails(this);
    customerdetails->setIndex(index);
    connect(customerdetails,SIGNAL(completed()),this,SLOT(updateTableView()));
    customerdetails->exec();
}

void Customer::updateTableView()
{
    model->select();
    emit changeOccured();
}
