#ifndef PRODUCT_H
#define PRODUCT_H

#include <QDialog>
#include <QtSql>
#include <QPointer>
#include <QMessageBox>

namespace Ui {
class Product;
}

class Product : public QDialog
{
    Q_OBJECT
    
public:
    explicit Product(QWidget *parent = 0);
    ~Product();

    void deleteProduct();
    
private slots:
    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_pushButton_add_clicked();

    void on_pushButton_remove_clicked();

    void on_pushButton_edit_clicked();

public slots:
    void updateTableView();

signals:
    void changeOccured();

private:
    Ui::Product *ui;
    QPointer<QSqlTableModel> model;
    QSqlDatabase db;
};

#endif // PRODUCT_H
