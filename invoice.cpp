#include "invoice.h"
#include "ui_invoice.h"
#include "invoicedetails.h"
#include "addinvoice.h"

Invoice::Invoice(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Invoice)
{
    ui->setupUi(this);

    db = QSqlDatabase::database("ouatelse");
    if (db.isOpen()) {
        model = new QSqlTableModel(this, db);
        model->setTable("oua_facture");
        model->select();
        ui->tableView->setModel(model);
        ui->tableView->hideColumn(0);
    } else {
        if (!model.isNull()) {
            model->clear();
        }
    }
}

Invoice::~Invoice()
{
    delete ui;
}

void Invoice::deleteInvoice()
{
    QSqlQuery query(db);
    QModelIndex currentIndex = ui->tableView->currentIndex();
    int id = currentIndex.sibling(currentIndex.row(),0).data().toInt() ;

    if (db.isOpen()) {
        query.prepare("delete from oua_facture where oua_facture.id = :id") ;
        query.bindValue(":id", id);
        query.exec();
        updateTableView();
    } else{
        QMessageBox::warning(this, tr("Error connection"), tr("Unable to connect to database."), QMessageBox::Ok);
    }
}

void Invoice::on_pushButton_add_clicked()
{
    QPointer<addInvoice> addinvoice = new addInvoice(this);
    connect(addinvoice,SIGNAL(completed()),this,SLOT(updateTableView()));
    addinvoice->exec();
}

void Invoice::on_pushButton_remove_clicked()
{
    QPointer<QItemSelectionModel> select = ui->tableView->selectionModel();
    if (select->hasSelection()) {
        QMessageBox msgBox;
        msgBox.setText("An invoice will be deleted.");
        msgBox.setInformativeText("Do you want to continue?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);

        switch (msgBox.exec()) {
        case QMessageBox::Yes:
            deleteInvoice();
            break;
        case QMessageBox::No:
            break;
        default:
            break;
        }
    } else {
        QMessageBox::warning(this, tr("Nothing selected"), tr("No row selected, please try again."), QMessageBox::Ok);
    }
}

void Invoice::on_pushButton_edit_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
    QPointer<invoiceDetails> invoicedetails = new invoiceDetails(this);
    invoicedetails->setIndex(index);
    connect(invoicedetails,SIGNAL(completed()),this,SLOT(updateTableView()));
    invoicedetails->exec();
}

void Invoice::on_tableView_doubleClicked(const QModelIndex &index)
{
    QPointer<invoiceDetails> invoicedetails = new invoiceDetails(this);
    invoicedetails->setIndex(index);
    connect(invoicedetails,SIGNAL(completed()),this,SLOT(updateTableView()));
    invoicedetails->exec();
}

void Invoice::updateTableView()
{
    model->select();
    emit changeOccured();
}
