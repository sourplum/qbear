#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    ui->comboBox_locale->addItem("English (en_US)","en");
    ui->comboBox_locale->addItem("French, France (fr_FR)","fr");

    ui->lineEdit_hostname->setText(settings.value("database/hostname").toString());
    ui->lineEdit_name->setText(settings.value("database/name").toString());
    ui->lineEdit_username->setText(settings.value("database/username").toString());
    ui->lineEdit_password->setText(settings.value("database/password").toString());

    ui->comboBox_locale->setCurrentIndex(ui->comboBox_locale->findData(settings.value("locale/lang").toString()));
}

Settings::~Settings()
{
    delete ui;
}

void Settings::on_listWidget_currentRowChanged(int currentRow)
{
    ui->stackedWidget->setCurrentIndex(currentRow);
}

void Settings::on_buttonBox_accepted()
{
    db = QSqlDatabase::database("ouatelse");

    settings.setValue("database/hostname",ui->lineEdit_hostname->text());
    settings.setValue("database/name",ui->lineEdit_name->text());
    settings.setValue("database/username",ui->lineEdit_username->text());
    settings.setValue("database/password",ui->lineEdit_password->text());

    settings.setValue("locale/lang", ui->comboBox_locale->itemData(ui->comboBox_locale->currentIndex()));

    if (db.isOpenError()) {
        db.setHostName(settings.value("database/hostname").toString());
        db.setDatabaseName(settings.value("database/name").toString());
        db.setUserName(settings.value("database/username").toString());
        db.setPassword(settings.value("database/password").toString());
    }
}

void Settings::on_buttonBox_clicked(QAbstractButton *button)
{
    switch (ui->buttonBox->standardButton(button)) {
    case QDialogButtonBox::Apply:
        if (ui->stackedWidget->currentWidget() == ui->page_database) {
            settings.setValue("database/hostname",ui->lineEdit_hostname->text());
            settings.setValue("database/name",ui->lineEdit_name->text());
            settings.setValue("database/username",ui->lineEdit_username->text());
            settings.setValue("database/password",ui->lineEdit_password->text());
        }
        else if (ui->stackedWidget->currentWidget() == ui->page_general) {
            settings.setValue("locale/lang", ui->comboBox_locale->itemData(ui->comboBox_locale->currentIndex()));
        }
        break;
    case QDialogButtonBox::Reset:
        if (ui->stackedWidget->currentWidget() == ui->page_database) {
            ui->lineEdit_hostname->clear();
            ui->lineEdit_name->clear();
            ui->lineEdit_username->clear();
            ui->lineEdit_password->clear();
        }
        else if (ui->stackedWidget->currentWidget() == ui->page_general) {
            ui->comboBox_locale->setCurrentIndex(0);
        }
        break;
    case QDialogButtonBox::RestoreDefaults:
        if (ui->stackedWidget->currentWidget() == ui->page_database) {
            ui->lineEdit_hostname->setText(settings.value("database/hostname").toString());
            ui->lineEdit_name->setText(settings.value("database/name").toString());
            ui->lineEdit_username->setText(settings.value("database/username").toString());
            ui->lineEdit_password->setText(settings.value("database/password").toString());
        }
        else if (ui->stackedWidget->currentWidget() == ui->page_general) {
            ui->comboBox_locale->setCurrentIndex(ui->comboBox_locale->findData(settings.value("locale/lang").toString()));
        }
        break;
    default:
        break;
    }
}
