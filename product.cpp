#include "product.h"
#include "ui_product.h"
#include "productdetails.h"
#include "addproduct.h"

Product::Product(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Product)
{
    ui->setupUi(this);

    db = QSqlDatabase::database("ouatelse");
    if (db.isOpen()) {
        model = new QSqlTableModel(this, db);
        model->setTable("oua_produit");
        model->select();
        ui->tableView->setModel(model);
        ui->tableView->hideColumn(0);
    } else {
        if (!model.isNull()) {
            model->clear();
        }
    }
}

Product::~Product()
{
    delete ui;
}

void Product::deleteProduct()
{
    QSqlQuery query(db);
    QModelIndex currentIndex = ui->tableView->currentIndex();
    int id = currentIndex.sibling(currentIndex.row(),0).data().toInt() ;

    if (db.isOpen()) {
        query.prepare("delete from oua_produit where oua_produit.id = :id") ;
        query.bindValue(":id", id);
        query.exec();
        updateTableView();
    } else{
        QMessageBox::warning(this, tr("Error connection"), tr("Unable to connect to database."), QMessageBox::Ok);
    }
}

void Product::on_tableView_doubleClicked(const QModelIndex &index)
{
    QPointer<productDetails> productdetails = new productDetails(this);
    productdetails->setIndex(index);
    connect(productdetails,SIGNAL(completed()),this,SLOT(updateTableView()));
    productdetails->exec();
}

void Product::on_pushButton_add_clicked()
{
    QPointer<addProduct> addproduct = new addProduct(this);
    connect(addproduct,SIGNAL(completed()),this,SLOT(updateTableView()));
    addproduct->exec();
}

void Product::on_pushButton_remove_clicked()
{
    QPointer<QItemSelectionModel> select = ui->tableView->selectionModel();
    if (select->hasSelection()) {
        QMessageBox msgBox;
        msgBox.setText("A product will be deleted.");
        msgBox.setInformativeText("Do you want to continue?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);

        switch (msgBox.exec()) {
        case QMessageBox::Yes:
            deleteProduct();
            break;
        case QMessageBox::No:
            break;
        default:
            break;
        }
    } else {
        QMessageBox::warning(this, tr("Nothing selected"), tr("No row selected, please try again."), QMessageBox::Ok);
    }
}

void Product::on_pushButton_edit_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
    QPointer<productDetails> productdetails = new productDetails(this);
    productdetails->setIndex(index);
    connect(productdetails,SIGNAL(completed()),this,SLOT(updateTableView()));
    productdetails->exec();
}

void Product::updateTableView()
{
    model->select();
    emit changeOccured();
}
