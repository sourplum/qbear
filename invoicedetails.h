#ifndef INVOICEDETAILS_H
#define INVOICEDETAILS_H

#include <QDialog>
#include <QtSql>
#include <QPointer>
#include <QDebug>
#include "qbear.h"

namespace Ui {
class invoiceDetails;
}

enum class invoicetablename {
    ID,DATE,PAYMENT_TYPE,SALE,EMPLOYEE_ID,CUSTOMER_ID
};

class invoiceDetails : public QDialog
{
    Q_OBJECT

public:
    explicit invoiceDetails(QWidget *parent = 0);
    ~invoiceDetails();

    void setIndex(const QModelIndex &value);

    QVariant getData(invoicetablename tag);
private slots:
    void on_buttonBox_accepted();

signals:
    void completed();

private:
    Ui::invoiceDetails *ui;
    QModelIndex index;
    QPointer<QBear> qbear;
};

#endif // INVOICEDETAILS_H
