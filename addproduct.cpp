#include "addproduct.h"
#include "ui_addproduct.h"

addProduct::addProduct(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addProduct)
{
    ui->setupUi(this);
    qbear = new QBear();
}

addProduct::~addProduct()
{
    delete ui;
}

void addProduct::on_buttonBox_accepted()
{
    QSqlDatabase db = QSqlDatabase::database("ouatelse");
    if (db.isOpen()) {
        QSqlQuery q(db);
        q.prepare("INSERT INTO oua_produit (nom, designation, prix_achat, prix_vente, tva, code_ean) "
                  "VALUES (:name, :desciption, :buy, :sell, :tax, :ean)");
        q.bindValue(":name", ui->lineEdit_name->text());
        q.bindValue(":description", ui->textEdit_note->toPlainText());
        q.bindValue(":buy", ui->doubleSpinBox_buy->value());
        q.bindValue(":sell", ui->doubleSpinBox_sell->value());
        q.bindValue(":tax", ui->doubleSpinBox_tax->value());
        q.bindValue(":ean", ui->lineEdit_codeEAN->text());
        q.exec();
    }
    emit completed();
}

