#include "addinvoice.h"
#include "ui_addinvoice.h"

addInvoice::addInvoice(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addInvoice)
{
    ui->setupUi(this);
    qbear = new QBear();

    qbear->fillComboboxCustomer(ui->comboBox_customer);
    qbear->fillComboboxEmployee(ui->comboBox_employee);
    qbear->fillComboboxPaymentType(ui->comboBox_payment_type);
}

addInvoice::~addInvoice()
{
    delete ui;
}

void addInvoice::on_buttonBox_accepted()
{
    QSqlDatabase db = QSqlDatabase::database("ouatelse");
    if (db.isOpen()) {
        QSqlQuery q(db);
        q.prepare("INSERT INTO oua_facture (date, moyens_paiements_id, pourcentage_remise, salaries_id, clients_id) "
                  "VALUES (:date, :payment_type, :sale, :employee_id, :customer_id)");
        q.bindValue(":date",ui->kdatecombobox_date->date());
        q.bindValue(":payment_type",ui->comboBox_payment_type->currentIndex());
        q.bindValue(":sale",ui->spinBox_sale->value());
        q.bindValue(":employee_id", ui->comboBox_employee->itemData(ui->comboBox_employee->currentIndex()));
        q.bindValue(":customer_id", ui->comboBox_customer->itemData(ui->comboBox_customer->currentIndex()));
        q.exec();
    }
    emit completed();
}
