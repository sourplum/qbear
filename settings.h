#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QSettings>
#include <QListWidgetItem>
#include <QtSql>
#include <QAbstractButton>

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT
    
public:
    explicit Settings(QWidget *parent = 0);
    ~Settings();
    
private slots:

    void on_listWidget_currentRowChanged(int currentRow);

    void on_buttonBox_accepted();

    void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::Settings *ui;
    QSettings settings;
    QSqlDatabase db;
};

#endif // SETTINGS_H
