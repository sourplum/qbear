#include "employeedetails.h"
#include "ui_employeedetails.h"

employeeDetails::employeeDetails(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::employeeDetails)
{
    ui->setupUi(this);
    qbear = new QBear();
    db = QSqlDatabase::database("ouatelse");
}

employeeDetails::~employeeDetails()
{
    delete ui;
}

void employeeDetails::on_comboBox_country_currentIndexChanged(int index)
{
    qbear->fillComboboxCity(ui->comboBox_city, ui->comboBox_country->itemData(index).toInt());
}

void employeeDetails::setIndex(const QModelIndex &value)
{
    index = value;
    ui->lineEdit_code->setText(getData(employeetablename::ID).toString());
    ui->lineEdit_name->setText(getData(employeetablename::NAME).toString());
    ui->lineEdit_firstname->setText(getData(employeetablename::FIRSTNAME).toString());
    ui->lineEdit_address1->setText(getData(employeetablename::ADDRESS1).toString());
    ui->lineEdit_address2->setText(getData(employeetablename::ADDRESS2).toString());
    ui->lineEdit_email->setText(getData(employeetablename::EMAIL).toString());
    ui->lineEdit_phone->setText(getData(employeetablename::PHONE).toString());
    ui->lineEdit_mobile->setText(getData(employeetablename::MOBILE).toString());
    ui->kdatecombobox_birthday->setDate(getData(employeetablename::BIRTHDAY).toDate());
    ui->textEdit_note->setText(getData(employeetablename::NOTE).toString());
    ui->comboBox_civility->setCurrentIndex(getData(employeetablename::CIVILITY).toInt());
    ui->lineEdit_login->setText(getData(employeetablename::LOGIN).toString());
    ui->lineEdit_password->setText(getData(employeetablename::PASSWORD).toString());

    qbear->fillComboboxCountry(ui->comboBox_country);
    ui->comboBox_country->setCurrentIndex(ui->comboBox_country->findData(getData(employeetablename::NATIONALITY).toInt()));

    qbear->fillComboboxCity(ui->comboBox_city, ui->comboBox_country->itemData(ui->comboBox_country->currentIndex()).toInt());
    ui->comboBox_city->setCurrentIndex(ui->comboBox_city->findData(getData(employeetablename::CITY).toInt()));

    qbear->fillComboboxStore(ui->comboBox_store);
    ui->comboBox_store->setCurrentIndex(ui->comboBox_store->findData(getData(employeetablename::STORE).toInt()));

    qbear->fillComboboxRole(ui->comboBox_role);
    ui->comboBox_role->setCurrentIndex(ui->comboBox_store->findData(getData(employeetablename::ROLE).toInt()));
}

QVariant employeeDetails::getData(employeetablename tag)
{
    return index.sibling(index.row(),(int)tag).data();
}


void employeeDetails::on_buttonBox_accepted()
{
    if (db.isOpen()) {
        QSqlQuery q(db);
        q.prepare("UPDATE oua_salarie SET nom=:name, prenom=:firstname, login=:login, password=:password, adresse1=:addressFirst, adresse2=:addressSecond, fixe=:phone, portable=:mobile, mail=:mail, naissance=:birthday, notes=:notes, civilite_id=:idCivil, villes_id=:idCity, pays_id=:idNation, roles_id=:idRole, magasin_id=:idStore WHERE id=:id");
        q.bindValue(":id", ui->lineEdit_code->text());
        q.bindValue(":name", ui->lineEdit_name->text());
        q.bindValue(":firstname", ui->lineEdit_firstname->text());
        q.bindValue(":login", ui->lineEdit_login->text());
        q.bindValue(":password",ui->lineEdit_password->text());
        q.bindValue(":addressFirst", ui->lineEdit_address1->text());
        q.bindValue(":addressSecond", ui->lineEdit_address2->text());
        q.bindValue(":phone", ui->lineEdit_phone->text());
        q.bindValue(":mobile", ui->lineEdit_mobile->text());
        q.bindValue(":mail", ui->lineEdit_email->text());
        q.bindValue(":birthday", ui->kdatecombobox_birthday->date());
        q.bindValue(":notes", ui->textEdit_note->toPlainText());
        q.bindValue(":idCivil", ui->comboBox_civility->currentIndex());
        q.bindValue(":idCity", ui->comboBox_city->itemData(ui->comboBox_city->currentIndex()));
        q.bindValue(":idNation", ui->comboBox_country->itemData(ui->comboBox_country->currentIndex()));
        q.bindValue(":idRole", ui->comboBox_role->itemData(ui->comboBox_role->currentIndex()));
        q.bindValue(":idStore", ui->comboBox_store->itemData(ui->comboBox_store->currentIndex()));
        q.exec();
    }
    emit completed();
}
