#ifndef ADDEMPLOYEE_H
#define ADDEMPLOYEE_H

#include <QDialog>
#include <QPointer>
#include <QSqlTableModel>
#include "qbear.h"

namespace Ui {
class addEmployee;
}

class addEmployee : public QDialog
{
    Q_OBJECT

public:
    explicit addEmployee(QWidget *parent = 0);
    ~addEmployee();
    
private slots:
    void on_buttonBox_accepted();

    void on_comboBox_country_currentIndexChanged(int index);

signals:
    void completed();

private:
    Ui::addEmployee *ui;
    QPointer<QBear> qbear;
    QSqlDatabase db;
};

#endif // ADDEMPLOYEE_H
