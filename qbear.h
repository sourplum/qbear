#ifndef QBEAR_H
#define QBEAR_H

#include <QtSql>
#include <QPointer>

class QBear : public QObject
{
    Q_OBJECT
public:
    explicit QBear(QObject *parent = 0);
    ~QBear();
    QString getEmployeeName(int id);
    QString getCustomerName(int id);
    QString getProductName(int id);
    QString getProductDescription(int id);
    QVariant getProductPrice(int id);
    int getBuyFrequency(int id);
    int getQuantityPerMonth(int customerId);

    void fillComboboxEmployee(QPointer<QComboBox> combobox);
    void fillComboboxCustomer(QPointer<QComboBox> combobox);
    void fillComboboxProduct(QPointer<QComboBox> combobox);
    void fillComboboxPaymentType(QPointer<QComboBox> combobox);
    void fillComboboxCountry(QPointer<QComboBox> combobox);
    void fillComboboxRole(QPointer<QComboBox> combobox);
    void fillComboboxStore(QPointer<QComboBox> combobox);
    void fillComboboxCity(QPointer<QComboBox> combobox, int countryId);
signals:

public slots:

private:
    QSqlDatabase db;
};

#endif // QBEAR_H
