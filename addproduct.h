#ifndef ADDPRODUCT_H
#define ADDPRODUCT_H

#include <QDialog>
#include <QtSql>
#include <QPointer>
#include <QDebug>
#include "qbear.h"

namespace Ui {
class addProduct;
}

class addProduct : public QDialog
{
    Q_OBJECT

public:
    explicit addProduct(QWidget *parent = 0);
    ~addProduct();

signals:
    void completed();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::addProduct *ui;
    QPointer<QBear> qbear;
};

#endif // ADDPRODUCT_H
