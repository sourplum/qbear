#include "addemployee.h"
#include "ui_addemployee.h"

addEmployee::addEmployee(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addEmployee)
{
    ui->setupUi(this);
    qbear = new QBear();

    qbear->fillComboboxCountry(ui->comboBox_country);
    qbear->fillComboboxRole(ui->comboBox_role);
    qbear->fillComboboxStore(ui->comboBox_store);

    db = QSqlDatabase::database("ouatelse");
}

addEmployee::~addEmployee()
{
    delete ui;
}

void addEmployee::on_buttonBox_accepted()
{
    if (db.isOpen()) {
        QSqlQuery q(db);
        q.prepare("INSERT INTO oua_salarie (nom, prenom, login, password, adresse1, adresse2, fixe, portable, mail, naissance, notes, civilite_id, villes_id, pays_id, roles_id, magasin_id) "
                  "VALUES (:name, :firstname, :login, :password, :addressFirst, :addressSecond, :phone, :mobile, :mail, :birthday, :notes, :idCivil, :idCity, :idCountry, :idRole, :idStore)");
        q.bindValue(":name", ui->lineEdit_name->text());
        q.bindValue(":firstname", ui->lineEdit_firstname->text());
        q.bindValue(":login", ui->lineEdit_login->text());
        q.bindValue(":password",ui->lineEdit_password->text());
        q.bindValue(":addressFirst", ui->lineEdit_address1->text());
        q.bindValue(":addressSecond", ui->lineEdit_address2->text());
        q.bindValue(":phone", ui->lineEdit_phone->text());
        q.bindValue(":mobile", ui->lineEdit_mobile->text());
        q.bindValue(":mail", ui->lineEdit_email->text());
        q.bindValue(":birthday", ui->kdatecombobox_birthday->date());
        q.bindValue(":notes", ui->textEdit_note->toPlainText());
        q.bindValue(":idCivil", ui->comboBox_civility->currentIndex());
        q.bindValue(":idCity", ui->comboBox_city->itemData(ui->comboBox_city->currentIndex()));
        q.bindValue(":idCountry", ui->comboBox_country->itemData(ui->comboBox_country->currentIndex()));
        q.bindValue(":idRole", ui->comboBox_role->itemData(ui->comboBox_role->currentIndex()));
        q.bindValue(":idStore", ui->comboBox_store->itemData(ui->comboBox_store->currentIndex()));
        q.exec();
    }
    emit completed();
}

void addEmployee::on_comboBox_country_currentIndexChanged(int index)
{
    qbear->fillComboboxCity(ui->comboBox_city, ui->comboBox_country->itemData(index).toInt());
}
