#-------------------------------------------------
#
# Project created by QtCreator 2015-01-26T14:23:22
#
#-------------------------------------------------

QT       += core gui sql
LIBS     += -lkdeui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++11

TARGET = QBear
TEMPLATE = app


SOURCES +=\
    customer.cpp \
    invoice.cpp \
    login.cpp \
    main.cpp \
    mainwindow.cpp \
    product.cpp \
    about.cpp \
    addcustomer.cpp\
    settings.cpp \
    customerdetails.cpp \
    qbear.cpp \
    addproduct.cpp \
    productdetails.cpp \
    addinvoice.cpp \
    invoicedetails.cpp \
    employee.cpp \
    addemployee.cpp \
    employeedetails.cpp

HEADERS  += mainwindow.h \
    login.h \
    customer.h \
    product.h \
    invoice.h \
    about.h \
    addcustomer.h\
    settings.h \
    customerdetails.h \
    qbear.h \
    addproduct.h \
    productdetails.h \
    addinvoice.h \
    invoicedetails.h \
    employee.h \
    addemployee.h \
    employeedetails.h

FORMS    += mainwindow.ui \
    login.ui \
    customer.ui \
    product.ui \
    invoice.ui \
    about.ui \
    addcustomer.ui\
    settings.ui \
    customerdetails.ui \
    addproduct.ui \
    productdetails.ui \
    addinvoice.ui \
    invoicedetails.ui \
    employee.ui \
    addemployee.ui \
    employeedetails.ui

TRANSLATIONS += qbear_en.ts qbear_fr.ts

RESOURCES += \
    Resource.qrc

OTHER_FILES +=
