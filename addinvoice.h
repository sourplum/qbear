#ifndef ADDINVOICE_H
#define ADDINVOICE_H

#include <QDialog>
#include <QtSql>
#include <QPointer>
#include <QDebug>
#include "qbear.h"

namespace Ui {
class addInvoice;
}

class addInvoice : public QDialog
{
    Q_OBJECT

public:
    explicit addInvoice(QWidget *parent = 0);
    ~addInvoice();

signals:
    void completed();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::addInvoice *ui;
    QPointer<QBear> qbear;
};

#endif // ADDINVOICE_H
