#ifndef PRODUCTDETAILS_H
#define PRODUCTDETAILS_H

#include <QDialog>
#include <QtSql>
#include <QPointer>
#include <QDebug>
#include "qbear.h"

namespace Ui {
class productDetails;
}

enum class producttablename {
    ID,NAME,DESCRIPTION,BUYINGPRICE,SELLINGPRICE,TAXE,EAN
};

class productDetails : public QDialog
{
    Q_OBJECT

public:
    explicit productDetails(QWidget *parent = 0);
    ~productDetails();

    QModelIndex getIndex() const;
    void setIndex(const QModelIndex &value);
    QVariant getData(producttablename tag);

private slots:
    void on_buttonBox_accepted();

signals:
    void completed();

private:
    Ui::productDetails *ui;
    QModelIndex index;
    QPointer<QBear> qbear;
};

#endif // PRODUCTDETAILS_H
