#include "employee.h"
#include "addemployee.h"
#include "employeedetails.h"
#include "ui_employee.h"

Employee::Employee(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Employee)
{
    ui->setupUi(this);
    
    db = QSqlDatabase::database("ouatelse");
    if (db.isOpen()) {
        model = new QSqlTableModel(this, db);
        model->setTable("oua_salarie");
        model->select();
        ui->tableView->setModel(model);
        ui->tableView->hideColumn(0);
    } else {
        if (!model.isNull()) {
            model->clear();
        }
    }
}

Employee::~Employee()
{
    delete ui;
}

void Employee::deleteEmployee()
{
    QSqlQuery query(db);
    QModelIndex currentIndex = ui->tableView->currentIndex();
    int id = currentIndex.sibling(currentIndex.row(),0).data().toInt() ;

    if (db.isOpen()) {
        query.prepare("delete from oua_salarie where oua_salarie.id = :id") ;
        query.bindValue(":id", id);
        query.exec();
        updateTableView();
    } else{
        QMessageBox::warning(this, tr("Error connection"), tr("Unable to connect to database."), QMessageBox::Ok);
    }
}

void Employee::on_tableView_doubleClicked(const QModelIndex &index)
{
    QPointer<employeeDetails> employeedetails = new employeeDetails(this);
    employeedetails->setIndex(index);
    connect(employeedetails,SIGNAL(completed()),this,SLOT(updateTableView()));
    employeedetails->exec();
}

void Employee::on_pushButton_add_clicked()
{
    QPointer<addEmployee> addemployee = new addEmployee(this);
    connect(addemployee,SIGNAL(completed()),this,SLOT(updateTableView()));
    addemployee->exec();
}

void Employee::on_pushButton_remove_clicked()
{
    QPointer<QItemSelectionModel> select = ui->tableView->selectionModel();
    if (select->hasSelection()) {
        QMessageBox msgBox;
        msgBox.setText("An employee will be deleted.");
        msgBox.setInformativeText("Do you want to continue?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);

        switch (msgBox.exec()) {
        case QMessageBox::Yes:
            deleteEmployee();
            break;
        case QMessageBox::No:
            break;
        default:
            break;
        }
    } else {
        QMessageBox::warning(this, tr("Nothing selected"), tr("No row selected, please try again."), QMessageBox::Ok);
    }
}

void Employee::on_pushButton_edit_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
    QPointer<employeeDetails> employeedetails = new employeeDetails(this);
    employeedetails->setIndex(index);
    connect(employeedetails,SIGNAL(completed()),this,SLOT(updateTableView()));
    employeedetails->exec();
}

void Employee::updateTableView()
{
    model->select();
    emit changeOccured();
}
