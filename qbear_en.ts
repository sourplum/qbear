<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="about.ui" line="14"/>
        <source>About QBear</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="60"/>
        <source>QBear</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="67"/>
        <source>Version 1.0</source>
        <oldsource>Version 0.2</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="74"/>
        <source>Using Qt 4.8.6</source>
        <oldsource>Using Qt 4.8</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="104"/>
        <source>About</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="110"/>
        <source>Enterprise Management</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="117"/>
        <source>2014-2015</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="124"/>
        <location filename="about.ui" line="127"/>
        <source>https://bitbucket.org/cedbonhomme/qbear</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="135"/>
        <source>Authors</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="170"/>
        <source>Mathilde Guedon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="156"/>
        <source>Cedric Bonhomme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="163"/>
        <source>Clement Binois</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="184"/>
        <source>Thomas Francisco</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="177"/>
        <source>Oliver Chedeville-Lebouc</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Customer</name>
    <message>
        <location filename="customer.ui" line="14"/>
        <source>Customer Managment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customer.ui" line="29"/>
        <source>Add Customer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customer.ui" line="66"/>
        <source>Remove Customer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customer.ui" line="41"/>
        <source>Edit Customer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customer.cpp" line="45"/>
        <source>Error connection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customer.cpp" line="45"/>
        <source>Unable to connect to database.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customer.cpp" line="76"/>
        <source>Nothing selected</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customer.cpp" line="76"/>
        <source>No row selected, please try again.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Employee</name>
    <message>
        <location filename="employee.ui" line="14"/>
        <source>Employee Managment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employee.ui" line="29"/>
        <source>Add Employee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employee.ui" line="41"/>
        <source>Edit Employee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employee.ui" line="66"/>
        <source>Remove Employee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employee.cpp" line="43"/>
        <source>Error connection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employee.cpp" line="43"/>
        <source>Unable to connect to database.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employee.cpp" line="82"/>
        <source>Nothing selected</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employee.cpp" line="82"/>
        <source>No row selected, please try again.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Invoice</name>
    <message>
        <location filename="invoice.ui" line="14"/>
        <source>Invoice Management</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoice.ui" line="29"/>
        <source>Add Invoice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoice.ui" line="66"/>
        <source>Remove Invoice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoice.ui" line="41"/>
        <source>Edit Invoice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoice.cpp" line="43"/>
        <source>Error connection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoice.cpp" line="43"/>
        <source>Unable to connect to database.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoice.cpp" line="74"/>
        <source>Nothing selected</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoice.cpp" line="74"/>
        <source>No row selected, please try again.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="login.ui" line="14"/>
        <source>Login</source>
        <translation></translation>
    </message>
    <message>
        <location filename="login.ui" line="63"/>
        <source>&amp;Login:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="login.ui" line="73"/>
        <location filename="login.ui" line="90"/>
        <source>admin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="login.ui" line="80"/>
        <source>Password:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="login.cpp" line="48"/>
        <source>Permission denied</source>
        <translation></translation>
    </message>
    <message>
        <location filename="login.cpp" line="48"/>
        <source>Possibly incorrect password, please try again.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <source>QBear</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="62"/>
        <source>Checkout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="79"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Checkout&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="113"/>
        <source>Me</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="140"/>
        <source>Date</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="189"/>
        <source>Client</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="246"/>
        <source>New Customer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="286"/>
        <source>Product</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="323"/>
        <source>Add</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="361"/>
        <source>Payment Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="371"/>
        <source>Discount</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="378"/>
        <source> %</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <source>TOTAL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="411"/>
        <source> €</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="465"/>
        <source>Acco&amp;unt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="472"/>
        <source>&amp;Customers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="478"/>
        <source>P&amp;roducts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="484"/>
        <source>In&amp;voices</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="490"/>
        <source>He&amp;lp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="496"/>
        <source>Settin&amp;gs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="502"/>
        <source>E&amp;mployees</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="516"/>
        <source>Main Toolbar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="551"/>
        <source>&amp;Login</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="567"/>
        <source>&amp;Disconnect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="580"/>
        <source>&amp;View Customers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="593"/>
        <source>&amp;View Products</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="603"/>
        <source>&amp;View Invoices</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="613"/>
        <source>&amp;About QBear</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="623"/>
        <source>&amp;Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="633"/>
        <source>&amp;View Employees</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="583"/>
        <source>View Customers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>id</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>Quantity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>Price</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Product</name>
    <message>
        <location filename="product.ui" line="14"/>
        <source>Product Management</source>
        <translation></translation>
    </message>
    <message>
        <location filename="product.ui" line="29"/>
        <source>Add Product</source>
        <translation></translation>
    </message>
    <message>
        <location filename="product.ui" line="66"/>
        <source>Remove Product</source>
        <translation></translation>
    </message>
    <message>
        <location filename="product.ui" line="41"/>
        <source>Edit Product</source>
        <translation></translation>
    </message>
    <message>
        <location filename="product.cpp" line="43"/>
        <source>Error connection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="product.cpp" line="43"/>
        <source>Unable to connect to database.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="product.cpp" line="82"/>
        <source>Nothing selected</source>
        <translation></translation>
    </message>
    <message>
        <location filename="product.cpp" line="82"/>
        <source>No row selected, please try again.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="settings.ui" line="14"/>
        <source>Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="36"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;General&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="43"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Language&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="71"/>
        <source>Available translations:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="84"/>
        <source>In order to change language, you must restart Qbear</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="111"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Appearance&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Database&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="146"/>
        <source>Configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="152"/>
        <source>Hostname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="162"/>
        <source>Database Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="172"/>
        <source>Username</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="182"/>
        <source>Password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="241"/>
        <source>General</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="251"/>
        <source>Appearance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="261"/>
        <source>Database</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>addCustomer</name>
    <message>
        <location filename="addcustomer.ui" line="14"/>
        <source>Add a customer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Add a customer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="54"/>
        <source>Code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="68"/>
        <source>Civility</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="76"/>
        <source>Mr</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="81"/>
        <source>Mrs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="86"/>
        <source>Ms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="94"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="101"/>
        <source>Customer NAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="108"/>
        <source>Firstname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="115"/>
        <source>Customer firstname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="122"/>
        <source>Email</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="132"/>
        <source>customer@host.com</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="139"/>
        <source>Birthday</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="146"/>
        <source>Phone</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="153"/>
        <source>0510213243</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="160"/>
        <source>Mobile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="167"/>
        <source>0642536475</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="189"/>
        <source>Adress 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="196"/>
        <source>Customer address 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="203"/>
        <source>Adress 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="210"/>
        <source>Customer address 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="217"/>
        <source>Country</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="224"/>
        <source>City</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addcustomer.ui" line="260"/>
        <source>Note</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>addEmployee</name>
    <message>
        <location filename="addemployee.ui" line="14"/>
        <source>Add an employee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Add an employee&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="39"/>
        <source>Code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="53"/>
        <source>Civility</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="61"/>
        <source>Mr</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="66"/>
        <source>Mrs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="71"/>
        <source>Ms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="79"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="86"/>
        <source>NAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="93"/>
        <source>Firstname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="100"/>
        <source>firstname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="107"/>
        <source>Email</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="117"/>
        <source>employee@host.com</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="124"/>
        <source>Birthday</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="134"/>
        <source>Phone</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="141"/>
        <source>0510213243</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="148"/>
        <source>Mobile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="155"/>
        <source>0642536475</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="174"/>
        <source>Adress 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="181"/>
        <source>main address</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="188"/>
        <source>Adress 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="195"/>
        <source>second address</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="202"/>
        <source>Country</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="216"/>
        <source>City</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="243"/>
        <source>Store</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="253"/>
        <source>Role</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="275"/>
        <source>Login</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="282"/>
        <source>login</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="289"/>
        <source>Password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="302"/>
        <source>password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addemployee.ui" line="324"/>
        <source>Note</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>addInvoice</name>
    <message>
        <location filename="addinvoice.ui" line="14"/>
        <source>Add Invoice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Add Invoice&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="54"/>
        <source>Code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="68"/>
        <source>Date</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="75"/>
        <source>Payment type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="86"/>
        <source>Cheque</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="91"/>
        <source>Change</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="96"/>
        <source>Credit Card</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="119"/>
        <source>Sale</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="126"/>
        <source> %</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="133"/>
        <source>Employee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addinvoice.ui" line="147"/>
        <source>Customer</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>addProduct</name>
    <message>
        <location filename="addproduct.ui" line="14"/>
        <source>Add a Product</source>
        <oldsource>Product Details</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Add a Product&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Product Details&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="54"/>
        <source>Code EAN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="68"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="75"/>
        <source>Product Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="94"/>
        <source>Buying Price</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="101"/>
        <source>Selling Price</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="108"/>
        <source>Taxes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="115"/>
        <location filename="addproduct.ui" line="122"/>
        <location filename="addproduct.ui" line="129"/>
        <source> €</source>
        <translation></translation>
    </message>
    <message>
        <location filename="addproduct.ui" line="146"/>
        <source>Note</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>customerDetails</name>
    <message>
        <location filename="customerdetails.ui" line="14"/>
        <source>Customer Details</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Customer Details&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="54"/>
        <source>Code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="68"/>
        <source>Civility</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="76"/>
        <source>Mr</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="81"/>
        <source>Mrs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="86"/>
        <source>Ms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="94"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="104"/>
        <source>Firstname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="114"/>
        <source>Email</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="128"/>
        <source>Birthday</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="135"/>
        <source>Phone</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="145"/>
        <source>Mobile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="170"/>
        <source>Adress 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="180"/>
        <source>Adress 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="190"/>
        <source>Country</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="197"/>
        <source>City</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="228"/>
        <source>Invoices</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="238"/>
        <source>Statistics</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="244"/>
        <source>Buy Frequency</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="257"/>
        <location filename="customerdetails.ui" line="277"/>
        <source> %</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="264"/>
        <source>Quantity Per Month</source>
        <translation></translation>
    </message>
    <message>
        <location filename="customerdetails.ui" line="285"/>
        <source>Note</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>employeeDetails</name>
    <message>
        <location filename="employeedetails.ui" line="14"/>
        <source>Employee Details</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Employee Details&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="39"/>
        <source>Code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="53"/>
        <source>Civility</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="61"/>
        <source>Mr</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="66"/>
        <source>Mrs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="71"/>
        <source>Ms</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="79"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="86"/>
        <source>NAME</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="93"/>
        <source>Firstname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="100"/>
        <source>firstname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="107"/>
        <source>Email</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="117"/>
        <source>employee@host.com</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="124"/>
        <source>Birthday</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="134"/>
        <source>Phone</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="141"/>
        <source>0510213243</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="148"/>
        <source>Mobile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="155"/>
        <source>0642536475</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="174"/>
        <source>Adress 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="181"/>
        <source>main address</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="188"/>
        <source>Adress 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="195"/>
        <source>second address</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="202"/>
        <source>Country</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="216"/>
        <source>City</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="243"/>
        <source>Store</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="253"/>
        <source>Role</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="275"/>
        <source>Login</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="282"/>
        <source>login</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="289"/>
        <source>Password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="302"/>
        <source>password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="employeedetails.ui" line="324"/>
        <source>Note</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>invoiceDetails</name>
    <message>
        <location filename="invoicedetails.ui" line="14"/>
        <source>Invoice Details</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Invoice Details&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="54"/>
        <source>Code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="68"/>
        <source>Date</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="75"/>
        <source>Payment type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="104"/>
        <source>Sale</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="111"/>
        <source> %</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="118"/>
        <source>Employee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="invoicedetails.ui" line="132"/>
        <source>Customer</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>productDetails</name>
    <message>
        <location filename="productdetails.ui" line="14"/>
        <source>Product Details</source>
        <oldsource>Customer Details</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Product Details&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="57"/>
        <source>Code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="71"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="93"/>
        <source>Buying Price</source>
        <translation></translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="100"/>
        <source>Selling Price</source>
        <translation></translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="107"/>
        <source>Taxes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="114"/>
        <location filename="productdetails.ui" line="121"/>
        <location filename="productdetails.ui" line="128"/>
        <source> €</source>
        <translation></translation>
    </message>
    <message>
        <location filename="productdetails.ui" line="145"/>
        <source>Note</source>
        <translation></translation>
    </message>
</context>
</TS>
