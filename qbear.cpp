#include "qbear.h"

QBear::QBear(QObject *parent) : QObject(parent)
{
    this->db = QSqlDatabase::database("ouatelse");
}

QBear::~QBear()
{

}

void QBear::fillComboboxEmployee(QPointer<QComboBox> combobox)
{
    if (db.isOpen()) {
        QSqlQuery query(db);
        combobox->clear();
        query.exec("Select CONCAT_WS(' ',nom,prenom), id from oua_salarie order by nom");
        while (query.next()) {
            combobox->addItem(query.value(0).toString(), query.value(1).toInt());
        }
        combobox->setCurrentIndex(-1);
    }
}



void QBear::fillComboboxCustomer(QPointer<QComboBox> combobox)
{
    if (db.isOpen()) {
        QSqlQuery query(db);
        combobox->clear();
        query.exec("Select CONCAT_WS(' ',nom,prenom), id from oua_client order by nom");
        while (query.next()) {
            combobox->addItem(query.value(0).toString(), query.value(1).toInt());
        }
        combobox->setCurrentIndex(-1);
    }
}

void QBear::fillComboboxProduct(QPointer<QComboBox> combobox)
{
    if (db.isOpen()) {
        QSqlQuery query(db);
        combobox->clear();
        query.exec("Select nom, id from oua_produit order by nom");
        while (query.next()) {
            combobox->addItem(query.value(0).toString(), query.value(1).toInt());
        }
        combobox->setCurrentIndex(-1);
    }
}

void QBear::fillComboboxPaymentType(QPointer<QComboBox> combobox)
{
    if (db.isOpen()) {
        QSqlQuery query(db);
        combobox->clear();
        query.exec("SELECT type, id FROM oua_moyen_de_paiement order by type");
        while (query.next()) {
            combobox->addItem(query.value(0).toString(),query.value(1).toInt());
        }
        combobox->setCurrentIndex(-1);
    }
}

void QBear::fillComboboxCountry(QPointer<QComboBox> combobox)
{
    if (db.isOpen()) {
        QSqlQuery query(db);
        combobox->clear();
        query.exec("Select libelle, id from oua_pays order by libelle");
        while (query.next()) {
            combobox->addItem(query.value(0).toString(), query.value(1).toInt());
        }
        combobox->setCurrentIndex(-1);
    }
}

void QBear::fillComboboxRole(QPointer<QComboBox> combobox)
{
    if (db.isOpen()) {
        QSqlQuery query(db);
        combobox->clear();
        query.exec("Select libelle, id from oua_roles order by libelle");
        while (query.next()) {
            combobox->addItem(query.value(0).toString(), query.value(1).toInt());
        }
        combobox->setCurrentIndex(-1);
    }
}

void QBear::fillComboboxStore(QPointer<QComboBox> combobox)
{
    if (db.isOpen()) {
        QSqlQuery query(db);
        combobox->clear();
        query.exec("SELECT CONCAT_WS(', ',oua_magasin.adresse, oua_ville.libelle), oua_magasin.id "
                   "FROM oua_magasin "
                   "INNER JOIN oua_ville ON oua_magasin.ville_id = oua_ville.id "
                   "WHERE oua_magasin.ville_id = oua_ville.id");
        while (query.next()) {
            combobox->addItem(query.value(0).toString(), query.value(1).toInt());
        }
        combobox->setCurrentIndex(-1);
    }
}

void QBear::fillComboboxCity(QPointer<QComboBox> combobox, int countryId)
{
    if(db.isOpen())
    {
        QSqlQuery query(db);
        combobox->clear();
        query.prepare("Select libelle, id from oua_ville where pays_id=:idPays order by libelle");
        query.bindValue(":idPays", countryId);
        query.exec();
        while (query.next()) {
            combobox->addItem(query.value(0).toString(), query.value(1).toInt());
        }
        combobox->setCurrentIndex(-1);
    }
}

int QBear::getBuyFrequency(int customerId){

    int frequency;
    if (db.isOpen()){
        QSqlQuery queryFrequency(db);
        queryFrequency.prepare("SELECT COUNT( DATE ) AS nbAchat, DATE "
                               "FROM oua_facture "
                               "WHERE clients_id = customerId "
                               "GROUP BY DATE "
                               "ORDER BY DATE "
                               "LIMIT 0 , 30");
        queryFrequency.bindValue(":idCustomer", customerId);
        queryFrequency.exec();

        while (queryFrequency.next()) {
            frequency = queryFrequency.value(0).toInt();
        }
    }
    return frequency;
}


int QBear::getQuantityPerMonth(int customerId){
    int quantityPerMonth;
    if (db.isOpen()){
        QSqlQuery queryQuantityPerMonth(db);
        queryQuantityPerMonth.prepare("SELECT MONTH( DATE ) AS MONTH , oua_facture_produit.quantite AS quantity FROM oua_facture "
                                      "INNER JOIN oua_facture_produit ON oua_facture.id = oua_facture_produit.factures_id "
                                      "WHERE oua_facture.clients_id =idCustomer "
                                      "GROUP BY MONTH "
                                      "ORDER BY MONTH "
                                      "LIMIT 0 , 12");
        queryQuantityPerMonth.bindValue(":idCustomer", customerId);
        queryQuantityPerMonth.exec();

        while (queryQuantityPerMonth.next()) {
            quantityPerMonth = queryQuantityPerMonth.value(0).toInt();
        }
    }
    return quantityPerMonth;
}

QString QBear::getEmployeeName(int id)
{
    QString employeeName;
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("Select CONCAT_WS(' ',nom,prenom), id from oua_salarie where id=:id order by nom");
        query.bindValue(":id", id);
        query.exec();
        while (query.next()) {
            employeeName = query.value(0).toString();
        }
    }
    return employeeName;
}

QString QBear::getCustomerName(int id)
{
    QString customerName;
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("Select CONCAT_WS(' ',nom,prenom), id from oua_client where id=:id order by nom");
        query.bindValue(":id", id);
        query.exec();
        while (query.next()) {
            customerName = query.value(0).toString();
        }
    }
    return customerName;
}

QString QBear::getProductName(int id)
{
    QString productName;
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT nom FROM oua_produit where id=:id");
        query.bindValue(":id", id);
        query.exec();
        while (query.next()) {
            productName = query.value(0).toString();
        }
    }
    return productName;
}

QString QBear::getProductDescription(int id)
{
    QString productDescription;
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT designation FROM oua_produit where id=:id");
        query.bindValue(":id", id);
        query.exec();
        while (query.next()) {
            productDescription = query.value(0).toString();
        }
    }
    return productDescription;
}

QVariant QBear::getProductPrice(int id)
{
    QVariant productPrice;
    if (db.isOpen()) {
        QSqlQuery query(db);
        query.prepare("SELECT prix_vente FROM oua_produit where id=:id");
        query.bindValue(":id", id);
        query.exec();
        while (query.next()) {
            productPrice = query.value(0);
        }
    }
    return productPrice;
}






